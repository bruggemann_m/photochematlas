%% VOC emission maps for each season

function VOCsum = runVOCmodel(windlimit,MW,flux)

% define parameters (only if VOC model was not already run before)
if mean(ismember({'wlim','molmass','VOC_flx'},who)) ~= 1
    
    global wlim molmass VOC_flx
    
    % give wind speed limit for the presence of an SML
    if nargin < 1, 
        wlim = 13;
    else
        wlim = windlimit;
    end

    % give molar mass of VOC
    if nargin < 2,
        molmass = 68.12;
    else
        molmass = MW; %in g/mol
    end

        % for isoprene: molmass = 68.12 g/mol
        % for total VOCs from SML samples: molmass = 99.41 g/mol (average of all signals)
        % for total VOCs from biofilm samples: molmass = 85.679 g/mol (average of all signals)
        % for unsaturated VOCs from SML samples: molmass = 93.30 g/mol (average of all signals)
        % for unsaturated VOCs from biofilm samples: molmass = 103.02 g/mol (average of all signals)

    % give emmission flux for VOC
    if nargin < 3
        VOC_flx = 6.190e7;
    else
        VOC_flx = flux; %in molecules mW-1 s-1
    end

        % emission values used for isoprene estimations:
        % for auth. SML sample:         6.190e7 molecules mW-1 s-1 (for EF 1.5; Ciuraru et al., EST, 2015)
        % for living biofilms (day 5):  3.710e7 molecules mW-1 s-1 (Brüggemann et al., Faraday Discuss., 2017)
        % for living biofilms (day 6):  4.788e7 molecules mW-1 s-1 (Brüggemann et al., Faraday Discuss., 2017)

        % emission values used for total VOC estimations:
        % for auth. SML sample:         213.64e7 molecules mW-1 s-1 (for EF 1.5; Ciuraru et al., EST, 2015)
        % for living biofilms (day 5):  156.896e7 molecules mW-1 s-1 (Brüggemann et al., Faraday Discuss., 2017)
        % for living biofilms (day 6):  420.962e7 molecules mW-1 s-1 (Brüggemann et al., Faraday Discuss., 2017)
        
        % emission values used for total unsaturated VOC estimations:
        % for auth. SML sample:         42.09e7 molecules mW-1 s-1 (for EF 1.5; Ciuraru et al., EST, 2015)
        % for living biofilms (day 5):  22.516e7 molecules mW-1 s-1 (Brüggemann et al., Faraday Discuss., 2017)
        % for living biofilms (day 6):  31.378e7 molecules mW-1 s-1 (Brüggemann et al., Faraday Discuss., 2017)
    
    display(' ')
    display('! New parameters loaded ! ')
    display('The following parameters will be used for the calculation:')
    display(['wlim: ', num2str(wlim), ' m/s'])
    display(['molmass: ', num2str(molmass),' g/mol'])
    display(['VOC_flx: ', num2str(VOC_flx / 1e7), 'e7 molec./(mW s)'])
    display(' ')
    
else
    display(' ')
    display('The following parameters will be used for the calculation:')
    display(['wlim: ', num2str(wlim), ' m/s'])
    display(['molmass: ', num2str(molmass),' g/mol'])
    display(['VOC_flx: ', num2str(VOC_flx / 1e7), 'e7 molec./(mW s)'])
    display(' ')

end
    
%% run calculations

VOCsum = VOC_seasonal_flux;

end