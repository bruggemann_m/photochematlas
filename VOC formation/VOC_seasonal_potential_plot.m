%% create subplots for different seasons of isoprene emissions
%  sum of VOC emissions is saved for each season in 'VOCsum'
%  maximum emissions for each season is saved in 'scale'

function VOC_seasonal_potential_plot(windlimit)

global colmap wlim molmass VOC_flx

molmass = 1;
VOC_flx = 1;

if nargin < 1, 
    wlim = 13; 
else
    wlim = windlimit; 
end

figure('color',[1 1 1],'units','centimeters','position',[3 3 18.3 12],...
    'paperunits','centimeters','paperposition',[0 0 18.3 12],...
    'papersize',[18.3 12])

h(1,1) = subplot(2,2,1);
[~,~,scale(1,1)] = VOC_map_potential(1,3,'Jan-Mar');
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,2) = subplot(2,2,2);
[~,~,scale(1,2)] = VOC_map_potential(4,6,'Apr-Jun');
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,3) = subplot(2,2,3);
[~,~,scale(1,3)] = VOC_map_potential(7,9,'Jul-Sep');
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

h(1,4) = subplot(2,2,4);
[~,~,scale(1,4)] = VOC_map_potential(10,12,'Oct-Dec');
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

% normalize all plots to the maximum emissions for all seasons
%max_val = max(scale);
max_val = 4e5;

for i=1:4
    set(h(1,i),'CLim',[0 max_val]);
end
clear i

% create colorbar
axes('position',[0.13 0.10 0.775 0.01],'color','none','xcolor','none','ycolor','none','xtick',[],'ytick',[])
caxis([0 max_val/1e4]);
colormap(colmap);
h(1,5) = colorbar('h','north','fontsize',10);
set(get(h(1,5),'title'),'string','\it�\rm_{photo} / 10^4 mW s cm^{-2} day^{-1}','fontsize',11)

% uncomment the following lines if colorbar is going off scale
set(h(1,5),'ticks',[0:max_val/1e4/10:max_val/1e4])
newlabels = get(h(1,5),'ticklabels'); 
maxlabel = cellstr(['\geq',char(newlabels(end,:))]);
newlabels(end,:) = maxlabel;
set(h(1,5),'ticklabels',cell(newlabels))

%saveplot;
end