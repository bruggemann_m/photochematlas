function [mix_ratios,scale] = VOC_ppt(start,stop,givename)

% calculates mixing ratios based on the modeled VOC fluxes, assuming an
% instantaneous mixing of the air masses in the planetary boundary layer
% and no transport between the grid points

global Lat Lon ppt_colors

currentfolder = evalin('base','currentfolder');
load([currentfolder, '\input data\pbl.mat'],'pbl');

% define parameters if no arguments are given
if nargin < 1, start=1; end
if nargin < 2, stop=12; end
if nargin < 3, givename=[num2str(start), '-' num2str(stop)]; end

% define how to proceed when start>stop
if start>stop
    dummy(:,:,1:13-start) = pbl(1:180,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = pbl(1:180,:,1:stop);
    pbl = nanmean(dummy,3);
else
    pbl = nanmean(pbl(1:180,:,start:stop),3);
end

%% calculation of VOC concentration in the air:

% [VOC] = [VOC_flux]*[area of grid point]*[time] / [volume of air]
%       = [VOC_flux]*[area of grid point]*[time] / {[boundary layer height]*[area of grid point]}
%       = [VOC_flux]*[time] / [boundary layer height]

% since 'VOC_map' gives average VOC emissions for a day, the equation becomes:
%       = [VOC emissions] / [boundary layer height]


[VOCmap,~,~] = VOC_map(start,stop);
title(['VOC flux / molec. cm^{-2} day^{-1}         season: ', num2str(start), ' - ', num2str(stop)])

mix_ratios = (VOCmap * 1e4) ./ pbl;  % factor 1e4 to convert molec./cm^2 to molec./m^2

% now we have a map of the daily averaged VOC concentrations for the selected period in molec./m3

%% convert to ppt

Temp = 20; %�C
press = 1.01325e5; %Pa
Avogadro = 6.022e23; %molecules/mol
R = 8.314; % m^3 Pa K-1 mol-1
V = 1; %m^3

numberofmolec = (press * V) / (R * (Temp+273.15))*Avogadro; %number of molecules/m3 for an ideal gas

mix_ratios = mix_ratios / numberofmolec * 1e12; % factor 1e12 to give mix. ratios in pptv

%% plot output using the m_map toolbox

%figure('color',[1 1 1])
m_pcolor(Lon,Lat,mix_ratios);shading flat;colormap(ppt_colors);   %plots VOC mixing ratios per day (in ppt)
m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);
%m_grid('xtick',[-180 -90 0 90 180]);
scale = max(caxis);

% please uncomment the following lines when running VOC_map individually:
% h=colorbar('h','eastoutside');
% set(get(h,'title'),'string','VOC mix. ratios / pptv','rotation',90,...
%     'fontsize',11,'position',[55.75 93.12]);

title(char(givename));

end