% calculate transformation maps which give global VOC emissions when
% multiplied by VOC production values from the lab
function [potential,mol_sum] = VOC_monthly_potential(wlim)

if nargin < 1, wlim =13; end

potential = zeros(180,360,12);    %transformation matrix for VOC emissions (in s mW cm-2 day-1)
mol_sum = zeros(1,12);          %transformation vector for sum of emissions (in mol / (molecules mW-1 s-1))

for i = 1:12
    [potential(:,:,i),mol_sum(:,i),~] = VOC_map_potential(i,i,wlim);
end

end