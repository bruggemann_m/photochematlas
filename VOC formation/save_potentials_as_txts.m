wlim = 'wlim13';    % adjust according to the wind speed limit

potential(isnan(potential)) = -9999;

jan = flipud(potential(:,:,1));
feb = flipud(potential(:,:,2));
mar = flipud(potential(:,:,3));
apr = flipud(potential(:,:,4));
may = flipud(potential(:,:,5));
jun = flipud(potential(:,:,6));
jul = flipud(potential(:,:,7));
aug = flipud(potential(:,:,8));
sep = flipud(potential(:,:,9));
oct = flipud(potential(:,:,10));
nov = flipud(potential(:,:,11));
dec = flipud(potential(:,:,12));

save([outputfolder '\VOC formation potential\jan_' wlim '.txt'],'jan','-ascii')
save([outputfolder '\VOC formation potential\feb_' wlim '.txt'],'feb','-ascii')
save([outputfolder '\VOC formation potential\mar_' wlim '.txt'],'mar','-ascii')
save([outputfolder '\VOC formation potential\apr_' wlim '.txt'],'apr','-ascii')
save([outputfolder '\VOC formation potential\may_' wlim '.txt'],'may','-ascii')
save([outputfolder '\VOC formation potential\jun_' wlim '.txt'],'jun','-ascii')
save([outputfolder '\VOC formation potential\jul_' wlim '.txt'],'jul','-ascii')
save([outputfolder '\VOC formation potential\aug_' wlim '.txt'],'aug','-ascii')
save([outputfolder '\VOC formation potential\sep_' wlim '.txt'],'sep','-ascii')
save([outputfolder '\VOC formation potential\oct_' wlim '.txt'],'oct','-ascii')
save([outputfolder '\VOC formation potential\nov_' wlim '.txt'],'nov','-ascii')
save([outputfolder '\VOC formation potential\dec_' wlim '.txt'],'dec','-ascii')