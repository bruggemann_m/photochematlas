%% create subplots for different seasons of isoprene emissions
%  sum of VOC emissions is saved for each season in 'VOCsum'
%  maximum emissions for each season is saved in 'scale'

function VOCppt = VOC_seasonal_ppt

global ppt_colors

figure('color',[1 1 1],'units','centimeters','position',[3 3 18.3 12],...
    'paperunits','centimeters','paperposition',[0 0 18.3 12],...
    'papersize',[18.3 12])

h(1,1) = subplot(2,2,1);
[VOCppt(:,:,1),scale(1,1)] = VOC_ppt(1,3,'Jan-Mar');
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,2) = subplot(2,2,2);
[VOCppt(:,:,2),scale(1,2)] = VOC_ppt(4,6,'Apr-Jun');
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,3) = subplot(2,2,3);
[VOCppt(:,:,3),scale(1,3)] = VOC_ppt(7,9,'Jul-Sep');
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

h(1,4) = subplot(2,2,4);
[VOCppt(:,:,4),scale(1,4)] = VOC_ppt(10,12,'Oct-Dec');
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

% normalize all plots to the maximum emissions for all seasons
for i=1:4
    set(h(1,i),'CLim',[0 max(scale)]);
end
clear i

% create colorbar
axes('position',[0.13 0.12 0.775 0.01],'color','none','xcolor','none','ycolor','none','xtick',[],'ytick',[])
caxis([0 max(scale)]);
colormap(ppt_colors);
h(1,5) = colorbar('h','north','fontsize',10);
set(get(h(1,5),'title'),'string','VOC mixing ratios / pptv','fontsize',11)

saveplot;
end