function [potential,potential_sum,scale] = VOC_map_potential(start,stop,givename)

global uvab wlim surf_eq srfc_area colmap Lat Lon

% define parameters if no arguments are given
if nargin < 1, start = 1; end
if nargin < 2, stop = 12; end
if nargin < 3, givename = 'photochem. potential'; end

currentfolder = evalin('base','currentfolder');
load([currentfolder, '\input data\wind.mat'],'wind');

if wlim==13
    load([currentfolder, '\input data\SML_seasonal.mat']);
else
    SML = SML_coverage(wlim);
end

% calculate averages for defined season
if start>stop
    dummy(:,:,1:13-start) = uvab(:,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = uvab(:,:,1:stop);
    uvab_avg = nanmean(dummy,3);
else
    uvab_avg = nanmean(uvab(:,:,start:stop),3); % averaged total UV irradiance/month
end

% convert data in 'SML' matrix to give normalized surfactant concentrations
SML(isnan(SML))=1;
SML(SML==0)=nan;            %no data available for these regions OR wind speed to high for SML
SML(SML==-1)=surf_eq(1,1);  %equivalents for oligotrophic regions
SML(SML==-2)=surf_eq(1,2);  %equivalents for mesotrophic regions
SML(SML==-3)=surf_eq(1,3);  %equivalents for eutrophic regions

% average SML data for selected season/month
if start>stop
    dummy(:,:,1:13-start) = SML(:,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = SML(:,:,1:stop);
    SML_avg = nanmean(dummy,3);
else
    SML_avg = nanmean(SML(:,:,start:stop),3);
end

% create transformation matrix in mW s cm-2 day-1
dayspermonth = 30;
potential = (uvab_avg.*(log(SML_avg)/log(surf_eq(1,3)))) / dayspermonth; % normalized to highest surfactant equivalent

%% include air-sea gas transfer velocity (k - U parameterizations)

lab_wind = 5.3e-2; %in m s-1
%k_lab = 3 + 0.1*lab_wind + 0.064*lab_wind^2 + 0.011*lab_wind^3; %WAN09 CO2 parameterization for lab values
k_lab = 8.2 + 0.014*lab_wind.^3; %McGillis04 CO2 parameterization

% average wind speeds for selected season/period
if start>stop
    dummy(:,:,1:13-start) = wind(:,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = wind(:,:,1:stop);
    wind_avg = nanmean(dummy,3);
else
    wind_avg = nanmean(wind(:,:,start:stop),3);
end

% create normalization matrix and multiply it with VOC production matrix
%k_map = (3 + 0.1 .* wind_avg + 0.064 .* wind_avg.^2 + 0.011 .* wind_avg.^3) ./ k_lab; %WAN09 CO2 parameterization
k_map = (8.2 + 0.014.*wind_avg.^3) / k_lab; %McGillis04 CO2 parameterization

potential = potential .* double(k_map);

%% calculate total emission of VOC from interfacial photochemistry in mol / (molecules mW-1 s-1)
if start>stop
    dummy = (nansum(nansum(potential.*srfc_area,1),2)*((12-start+1)*dayspermonth));
    potential_sum = dummy + (nansum(nansum(potential.*srfc_area,1),2)*(stop*dayspermonth));
else
    potential_sum = (nansum(nansum(potential.*srfc_area,1),2)*((stop-start+1)*dayspermonth));
end

display(potential_sum,char(['Global sum of �_photo / mW s']))

%% plot output using the m_map toolbox

% figure('color',[1 1 1])
m_pcolor(Lon,Lat,potential);shading flat;colormap(colmap);   %plots VOC flux per day
m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);
%m_grid('xtick',[-180 -90 0 90 180]);
scale = max(caxis);
% 
% % please uncomment the following lines when running VOC_map individually:
% h=colorbar('h','eastoutside');
% set(get(h,'title'),'string','photochem. potential \it� \rm/ mW s cm^{-2} day^{-1}','rotation',90,...
%     'fontsize',11,'position',[55.75 123.12]);

title(char(givename));
end