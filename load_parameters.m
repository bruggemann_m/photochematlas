%% load parameters for the VOC model runs

function load_parameters

global uvab surf_eq srfc_area colmap ppt_colors Lat Lon wlim

% define wind speed limit for the presence of an SML
wlim = 13;

% load uv irradiation data
currentfolder = evalin('base','currentfolder');
uvab = load([currentfolder, '\input data\UVab.mat'],'uvab');
uvab = flipud(uvab.uvab); % flip 'uvab' matrix upside-down to make it consistent to the other matrices

% calculate normalized surfactant equivalents (according to Wurl et al., 2011)
surf_eq = [320 503 663]; %for oligo-, meso-, and eutrophic regions respectively

% surface area of the Earth:
% calculate pixel areas with: pi/180*R^2*abs(sind(lat1)-sind(lat2))*abs(lon1-lon2)
% with 'R' = 6371 km (Earth radius) and 'abs(lon1-lon2)'=1 (since resolution = 1 degree)
% (explanation of the formula see: http://mathforum.org/library/drmath/view/63767.html)
Lat = 89.5:-1:-89.5;
Lon = -179.5:179.5;

srfc_area = zeros(180,360);
for i=1:179
    srfc_area(i,1) = pi/180*6371^2*abs(sind(Lat(1,i))-sind(Lat(1,i+1)));
end
srfc_area(180,1) = srfc_area(1,1);  %now we have the surface area in km^2 depending on latitude

for j=2:360
    srfc_area(:,j) = srfc_area(:,1);
end
srfc_area = srfc_area*1e10;         %now we have the surface area in cm^2 depending on latitude
clear i j

% load colormaps
colmap = load([currentfolder, '\input data\colors'],'colmap');
colmap = colmap.colmap;

ppt_colors = load([pwd, '\input data\colors'],'ppt_colors');
ppt_colors = ppt_colors.ppt_colors;

% initialize m_map
m_proj('Miller Cylindrical')

end