%% run VOC and SOA model in batch mode for olefines / isoprene
% parameters:

function [VOCdata,VOCsum,OAdata,OAmass,VOC_map,extraOA,rel_OAincrease,extraOAsum] = batch_VOCandSOA(yieldcase)

switch yieldcase
    case 'ole2'
        mw = [93.30, 103.02]; %g/mol, [SML samples, biofilm samples]
        flux = [41.05e7, 21.52e7, 30.34e7]; %for unsaturated VOCs
        
    case 'isop'
        mw = 68.12; %for isoprene, g/mol
        flux = [6.190e7, 3.710e7, 4.788e7]; %for isoprene
end

windlimit = [8, 10, 13];

%% run VOC model 
% VOC emissions from isoprene for different VOC fluxes and wind speed limits
VOCsum = zeros(1,5);
VOCdata.VOCsums = cell(3,4);
VOCdata.names = cell(3,4);

switch yieldcase
    case 'ole2'
        filename =({'unsatVOC_SML','unsatVOC_biof(d5)','unsatVOC_biof(d6)'});
    
    case 'isop'
        filename =({'isop_VOC_SML(orig)','isop_VOC_SML(corr)','isop_VOC_biof(d5)','isop_VOC_biof(d6)'});
end
        
for i = 1:length(windlimit)
    for j = 1:length(flux)
        if j <= 1, MW = mw(1,1); elseif j > 1, MW = mw(1,end); end
        VOCsum = runVOCmodel(windlimit(1,i),MW,flux(1,j));
        VOCdata.VOCsums{i,j} = VOCsum;
        VOCdata.names{i,j} = [char(filename(1,j)), '_wlim', num2str(windlimit(1,i))];
        saveplot(char(filename(1,j)),1);
        close(gcf)
        clear VOC_flx wlim molmass
    end
end

dummy = cell2mat(VOCdata.VOCsums);
VOCdata.minVOCannual = min(min(dummy(:,[5 10 15])));
VOCdata.maxVOCannual = max(max(dummy(:,[5 10 15])));

clear dummy i j

%% run SOA model 
% OA formation from isoprene for different VOC fluxes and wind speed limits
extraOAsum = zeros(1,5);
OAdata.OAsums = cell(3,4);
OAdata.names = cell(3,4);

switch yieldcase
    case 'ole2'
        filename =({'OLE2_SOA_SML','OLE2_SOA_biof(d5)','OLE2_SOA_biof(d6)'});
    
    case 'isop'
        filename =({'ISOP_SOA_SML','ISOP_SOA_biof(d5)','ISOP_SOA_biof(d6)'});
end


for i = 1:length(windlimit)
    for j = 1:length(flux)
        if j <= 1, MW = mw(1,1); elseif j > 1, MW = mw(1,end); end
        [OAmass,VOC_map,extraOA,rel_OAincrease,extraOAsum] = runSOAmodel(windlimit(1,i),MW,flux(1,j),yieldcase);
        OAdata.OAsums{i,j} = extraOAsum;
        OAdata.names{i,j} = [char(filename(1,j)), '_wlim', num2str(windlimit(1,i))];
        saveplot(char(filename(1,j)),1);
        close(gcf)
        clear VOC_flx wlim molmass
    end
end

dummy = cell2mat(OAdata.OAsums);
OAdata.minOAannual = min(min(dummy(:,[5 10 15])));
OAdata.maxOAannual = max(max(dummy(:,[5 10 15])));

%% save output (OAdata, VOCdata) in file
d = char(datetime('today','format','yyMMdd'));

currentfolder = evalin('base','currentfolder');
save([currentfolder, '\output\' d, '_VOC_and_OA_batch_data.mat'],'OAdata','VOCdata','flux','windlimit','mw')

clear i j d dummy