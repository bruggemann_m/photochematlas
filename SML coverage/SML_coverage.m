function SML = SML_coverage(wlim)
%% define presence of a surface microlayer (SML)
%  (similar to Wurl et al., Biogeoscience, 2011)

currentfolder = evalin('base','currentfolder');

if nargin < 1
    wlim = 13;  %if not otherwise stated wind speed limit is set to 13 m/s
end

% calculate trophic states
trophic = trophic_regions;

% load seasonal wind speeds
load([currentfolder, '\input data\wind.mat'],'wind')

% create conversion matrices
wind(wind>=wlim) = nan;
wind(~isnan(wind)) = 1;

% create monthly SML coverage data
SML=trophic;

for i=1:12
    SML(:,:,i)=SML(:,:,i).*wind(:,:,i);
end

%save('170322_SML_seasonal')
end