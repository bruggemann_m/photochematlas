%% create subplots for different seasons of isoprene emissions
%  sum of VOC emissions is saved for each season in 'VOCsum'
%  maximum emissions for each season is saved in 'scale'

function SML_seasonal(wlim)

if nargin < 1, wlim = 13; end

currentfolder = evalin('base','currentfolder');
load([currentfolder, '\input data\SML_colormap.mat']);

figure('color',[1 1 1],'units','centimeters','position',[3 3 18.3 12],...
    'paperunits','centimeters','paperposition',[0 0 18.3 12],...
    'papersize',[18.3 12])

h(1,1) = subplot(2,2,1);
SML_subfigure(1,3,wlim);
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,2) = subplot(2,2,2);
SML_subfigure(4,6,wlim);
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,3) = subplot(2,2,3);
SML_subfigure(7,9,wlim);
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

h(1,4) = subplot(2,2,4);
SML_subfigure(10,12,wlim);
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

% create colorbar
axes('position',[0.13 0.12 0.775 0.01],'color','none','xcolor','none','ycolor','none','xtick',[],'ytick',[])
caxis([-3 0]);
colormap(flipud(c));
h(1,5) = colorbar('h','north','ticks',[-3 -2 -1 0],'ticklabels',flip({'high','medium','low','none'}),'fontsize',10);
set(get(h(1,5),'title'),'string','SML coverage','fontsize',11)

% prompt = input('Do you want to save the figure? y/n [n]: ','s');
% if prompt == 'y'
%     d = char(datetime('today','format','yyMMdd'));
%     outputfolder = evalin('base','outputfolder');
%     filename = ['SML_seasonal_wlim' num2str(wlim)];
%     print([outputfolder d '_' filename '.pdf'],'-dpdf','-r600')
%     % uncomment the following line to save the figure also in .fig format
%     % saveas(gcf,[outputfolder d '_' filename '.fig'],'fig')
%     display(' ');
%     display(char(['Output was saved as: ' d '_' filename]));
%     display(' ');
%     display('Done!'); display(' ')
% end

end