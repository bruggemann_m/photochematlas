%% compare annual SML coverage at different wind speed limits

function SML_wlims

currentfolder = evalin('base','currentfolder');
load([currentfolder, '\input data\SML_colormap.mat']);

figure('color',[1 1 1],'units','centimeters','position',[3 3 8.9-0.1 14-0.1],...
    'paperunits','centimeters','paperposition',[0.1 0.1 8.9 14],...
    'papersize',[8.9 14])

h(1,1) = subplot(3,1,1);
SML_subfigure(1,12,8);
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])
title('\leq 8 m s^{-1}')

annotation('textbox',[0.1321 0.9339 0.0832 0.0514],'string','a)',...
    'edgecolor','none','fontsize',12,'fontweight','bold')

h(1,1) = subplot(3,1,2);
SML_subfigure(1,12,10);
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])
title('\leq 10 m s^{-1}')

annotation('textbox',[0.1321 0.6312 0.0832 0.0514],'string','b)',...
    'edgecolor','none','fontsize',12,'fontweight','bold')

h(1,1) = subplot(3,1,3);
SML_subfigure(1,12,13);
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])
title('\leq 13 m s^{-1}')

annotation('textbox',[0.1321 0.3305 0.0832 0.0514],'string','c)',...
    'edgecolor','none','fontsize',12,'fontweight','bold')

% create colorbar
axes('position',[0.13 0.07 0.775 0.01],'color','none','xcolor','none','ycolor','none','xtick',[],'ytick',[])
caxis([-3 0]);
colormap(flipud(c));
h(1,5) = colorbar('h','north','ticks',[-3 -2 -1 0],'ticklabels',flip({'high','medium','low','none'}),'fontsize',10);
set(get(h(1,5),'title'),'string','SML coverage','fontsize',11)

prompt = input('Do you want to save the figure? y/n [n]: ','s');
if prompt == 'y'
    d = char(datetime('today','format','yyMMdd'));
    outputfolder = evalin('base','outputfolder');
    filename = ['SML_seasonal_wlims'];
    print([outputfolder d '_' filename '.pdf'],'-dpdf','-r600')
    % uncomment the following line to save the figure also in .fig format
    % saveas(gcf,[outputfolder d '_' filename '.fig'],'fig')
    display(' ');
    display(char(['Output was saved as: ' d '_' filename]));
    display(' ');
    display('Done!'); display(' ')
end

end