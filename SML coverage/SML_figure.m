function SML_figure(start,stop,wlim)
%% prepare figure of seasonal SML coverage

if nargin < 1; start=1; end
if nargin < 2; stop=12; end
if nargin < 3; wlim=13; end

currentfolder = evalin('base','currentfolder');

load([currentfolder, '\input data\SML_colormap.mat']);

SML = SML_coverage(wlim);

SML(isnan(SML))=0;
avg_SML = nanmean(SML(:,:,start:stop),3);

figure('color',[1 1 1],'position',[360 500 560 420])
m_proj('Miller Cylindrical')
[Plg,Plt]=meshgrid([-180:179]-0.5,[-89:90]-0.5);

m_pcolor(Plg,Plt,flipud(avg_SML));shading flat;colormap(c);
m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);
%m_grid('xtick',[-180 -90 0 90 180],'xlabelrotation',1);
m_grid('xlabelrotation',1);

h=colorbar('h','eastoutside','ticks',[-3 -2 -1 0],'ticklabels',{'high','medium','low','n/a'},'fontsize',10);

month = {'Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec'};
title(['SML coverage for: ' cell2mat(month(1,start)) '-' cell2mat(month(1,stop)) ' (wind\leq' num2str(wlim) ' m/s)'])

prompt = input('Do you want to save the figure? y/n [n]: ','s');
if prompt == 'y'
    d = char(datetime('today','format','yyMMdd'));
    outputfolder = evalin('base','outputfolder');
    filename = ['SML_wlim' num2str(wlim) '_' cell2mat(month(1,start)) '-' cell2mat(month(1,stop))];
    print([outputfolder d '_' filename '.png'],'-dpng','-r300')
    saveas(gcf,[outputfolder d '_' filename '.fig'],'fig')
end

end