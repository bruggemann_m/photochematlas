function avg_SML = SML_subfigure(start,stop,wlim)
%% prepare figure of seasonal SML coverage

currentfolder = evalin('base','currentfolder');

load([currentfolder, '\input data\SML_colormap.mat']);

SML = SML_coverage(wlim);

SML(isnan(SML))=0;

if start>stop
    dummy(:,:,1:13-start) = SML(:,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = SML(:,:,1:stop);
    avg_SML = nanmean(dummy,3) .* -1;
else
    avg_SML = nanmean(SML(:,:,start:stop),3) .* -1;
end

[Plg,Plt]=meshgrid([-180:179]-0.5,[-89:90]-0.5);

m_pcolor(Plg,Plt,flipud(avg_SML));shading flat;colormap(c);
m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);
%m_grid('xtick',[-180 -90 0 90 180]);

month = {'Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec'};
title([cell2mat(month(1,start)) '-' cell2mat(month(1,stop))])

end