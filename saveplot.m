%% export plot to current save folder for publication

function saveplot(filename,directSaveOption)

global wlim

if nargin < 2,
    prompt = input('Do you want to save the current figure? y/n [n]: ','s');
elseif directSaveOption == 1,
    prompt = 'y';
end

if prompt == 'y',

outputfolder = evalin('base','outputfolder');
d = char(datetime('today','format','yyMMdd'));

if nargin < 1
    display(' ');
    filename = input('Please give a filename to save the output: ','s');
    display(' ');
    display(char(['Please wait... output will be saved as: ' d '_' filename '_wlim' num2str(wlim)]));
    display(' ');
end


print(char([outputfolder d '_' filename '_wlim' num2str(wlim) '.pdf']),'-dpdf','-r600');
% uncomment the following line to save figure also in .eps and .fig format
% print(char([outputfolder d '_' filename '_wlim' num2str(wlim) '.eps']),'-depsc','-r600');
% saveas(gcf,char([outputfolder d '_' filename '_wlim' num2str(wlim) '.fig']),'fig');

% if nargin < 2
%     prompt = input('Do you want to save the total VOC and/or OA emissions? y/n [n]: ','s');
% elseif directSaveOption == 1,
%     prompt = 'y';
%     display(' ');
%     display(char(['Output will be saved as: ' d '_' filename '_wlim' num2str(wlim)]));
%     display(' ');
% end

% if prompt == 'y',
%     
%     % save sum of VOC emissions in txt-file
%     % structure of the file is: Jan-Mar Apr-Jun Jul-Sep Oct-Dec Annual
%     if ismember({'VOCsum'},evalin('base','who')) == 1
%         VOCsum = evalin('base','VOCsum');
%         save(char([outputfolder d '_' filename '_VOCsum_wlim' num2str(wlim) '.txt']),'VOCsum','-ascii')
%     end
% 
%     % save sum of OA mass increase ('extra OA') in txt-file
%     % structure of the file is: Jan-Mar Apr-Jun Jul-Sep Oct-Dec Annual
%     if ismember({'extraOAsum'},evalin('base','who')) == 1
%         extraOAsum = evalin('base','extraOAsum');
%         save(char([outputfolder d '_' filename '_extraOA_wlim' num2str(wlim) '.txt']),'extraOAsum','-ascii')
%     end
%     
% end

display(' ');
display('Done!'); display(' ')

end
end