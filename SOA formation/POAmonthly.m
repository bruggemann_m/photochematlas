%% calculate total marine POA for each month

function [POA,E_POA,E_POAsum] = POAmonthly(Dp)

global srfc_area

if nargin < 1, Dp = 0.3; end % dominant particle diameter of accumulution mode (Lana, ACP, 2012 used 0.3), �m

POA = zeros(180,360);
E_POA = zeros(180,360);

for i = 1:12
    [POA(:,:,i),E_POA(:,:,i)] = POA_Dp(i,i,Dp); %in �g m-3
end

% calculate sum of POA for the each month in g
E_POAsum = zeros(1,12);

% load pbl data
currentfolder = evalin('base','currentfolder');
load([currentfolder, '\input data\pbl'])
pbl = pbl(1:180,:,:);

for i = 1:12
    E_POAsum(1,i) = nansum(nansum(E_POA(:,:,i).*srfc_area/1e4.*pbl(:,:,i),1),2)*1e-6; % global POA in g / month
end

end