%% create subplots for different seasons of isoprene emissions
%  sum of VOC emissions is saved for each season in 'VOCsum'
%  maximum emissions for each season is saved in 'scale'

function [OAmass,VOC,extraOA,rel_OAincrease,extraOAsum] = SOA_seasonal(agingTime,Temp,yieldcase)

global colmap

if nargin < 3, yieldcase = 'ole2'; end
if nargin < 2, Temp = 15; end           %in �C
if nargin < 1, agingTime = 12; end      %in hours

OH = 1.5e6; %molec. cm-3

OAmass = zeros(180,360,4);
VOC = zeros(180,360,4);
extraOA = zeros(180,360,4);
rel_OAincrease = zeros(180,360,4);
extraOAsum = zeros(1,5);

figure('color',[1 1 1],'units','centimeters','position',[3 3 18.3 12],...
    'paperunits','centimeters','paperposition',[0 0 18.3 12],...
    'papersize',[18.3 12])

h(1,1) = subplot(2,2,1);
[OAmass(:,:,1),VOC(:,:,1),extraOA(:,:,1),rel_OAincrease(:,:,1),extraOAsum(1,1)] = SOA_map(1,3,1,agingTime,Temp,OH,yieldcase);
title('Jan-Mar');
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,2) = subplot(2,2,2);
[OAmass(:,:,2),VOC(:,:,2),extraOA(:,:,2),rel_OAincrease(:,:,2),extraOAsum(1,2)] = SOA_map(4,6,1,agingTime,Temp,OH,yieldcase);
title('Apr-Jun');
m_grid('xtick',[-180 -90 0 90 180],'xticklabel',{});
set(gca,'position',get(gca,'position')-[0 -0.03 0 0])

h(1,3) = subplot(2,2,3);
[OAmass(:,:,3),VOC(:,:,3),extraOA(:,:,3),rel_OAincrease(:,:,3),extraOAsum(1,3)] = SOA_map(7,9,1,agingTime,Temp,OH,yieldcase);
title('Jul-Sep');
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

h(1,4) = subplot(2,2,4);
[OAmass(:,:,4),VOC(:,:,4),extraOA(:,:,4),rel_OAincrease(:,:,4),extraOAsum(1,4)] = SOA_map(10,12,1,agingTime,Temp,OH,yieldcase);
title('Oct-Dec');
m_grid('xtick',[-180 -90 0 90 180]);
set(gca,'position',get(gca,'position')-[0 -0.1 0 0])

% calculate the annual sum of emmissions
extraOAsum(1,5) = sum(extraOAsum(1,1:4),2);

% normalize all plots to the maximum emissions for all seasons
scale = 60; %set maximum for colorbar
for i=1:4
    set(h(1,i),'CLim',[0 scale]);
end
clear i

% create colorbar
axes('position',[0.13 0.10 0.775 0.01],'color','none','xcolor','none','ycolor','none','xtick',[],'ytick',[])
caxis([0 scale]);
colormap(colmap);
h(1,5) = colorbar('h','north','fontsize',10);
set(h(1,5),'ticks',[0:scale/10:scale])
newlabels = get(h(1,5),'ticklabels'); 
maxlabel = cellstr(['\geq',char(newlabels(end,:))]);
newlabels(end,:) = maxlabel;
set(h(1,5),'ticklabels',cell(newlabels))
set(get(h(1,5),'title'),'string','relative OA mass increase / %','fontsize',11)
end