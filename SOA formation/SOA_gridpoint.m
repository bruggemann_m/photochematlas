function [SOAmass,VOCfinal] = SOA_gridpoint(VOCconc, agingTime, Temp, POA, E_VOC, pbl, OH, printFigure, yields)

global molmass

% formation and partitioning of SOA is calculated according to the 1dim-VBS model (Donahue et al., EST, 2006)
% total org. aerosol mass is saved in C_OA

%% initial parameters
if nargin < 1, VOCconc = 0.01; end  % initial VOC concentration in ppb
if nargin < 2, agingTime = 12; end  % aging time in hours
if nargin < 3, Temp = 27; end       % ambient temperature in �C
if nargin < 4, POA = 0.3; end       % background org. aerosol in �g/m3 (treated as non-volatile)
if nargin < 5, E_VOC = 0; end       % photochem. emission flux in molec. cm-2 day-1
if nargin < 6, pbl = 500; end       % PBL height in m
if nargin < 7, OH = 1.5e6; end      % OH conc. in molec cm^-3
if nargin < 8, printFigure = 0; end % if set to 1, we get some figures at the end
if nargin < 9, yields = 'isop'; end      % select literature values for SOA yields (standard is ISOP yields in Hodzic, ACP, 2016)

Temp = 273.15 + Temp; % convert Temp to K

timeArray = [0:0.5:agingTime]*3600; % aging time in hours
dt = diff(timeArray(1:2));

% initialize array of Cs
C = [POA 0 0 0 0 0 0 0 0 0 0]; % total conc. in gas and aerosol phase in �g/m3

% initial partitioning parameters
C_OA = POA; % conc. in aerosol phase in �g/m3
Cstars = 10.^[-4:6]; %volatility bins at 300K (-4 is used as non-volatile)

%% precursor concentrations and SOA yields
% ppb conversion to molec. cm-3
press = 1.01325e5; %Pa
V = 1e-6; %m3 = 1 cm3
numberofmolec = (press * V) / (8.314 * Temp)*6.022e23; %number of molecules/cm3 for an ideal gas

VOC = VOCconc * 1e-9 * numberofmolec; % in molec./cm3

% photochemical VOC production (per second) and mixing of VOCs in the PBL
VOC_prod = E_VOC / (24*3600) / (pbl*100); % the emitted VOC are instantaneously diluted within the PBL, molec. cm-2 s-1

% define SOA mass yield parameters and reaction rate constant
switch yields
    case 'isop'
        % C bins -4  -3     -2     -1      0      1      2      3      4      5      6   % bin -4 is used for non-volatile POA
        alpha = [ 0,  0,     0.012, 0.013, 0.001, 0.1,   0.078, 0.097  0      0      0]; % yields for isoprene (Hodzic, 2016)
        kOH = 1e-10;    % cm3 molec^-1 s^-1 (reaction rate constant; see Hodzic, ACP, 2016)
    case 'ole2'
        alpha = [ 0,  0,     0,     0,     0.023, 0.044, 0.129, 0.375  0      0      0]; % yields for OLE2 (Tsimpidi, 2010)
        kOH = 1.1e-10;    % cm3 molec^-1 s^-1 (reaction rate constant = avg of kOHs for unsaturated compounds; see '170509_VOC_fluxes.xls')
    case 'ivoc'
        alpha = [ 0,  0,     0.315, 0.173, 0.046, 0.01,  0.007, 0.008  0      0      0]; % yields for IVOCs (C>12) (Hodzic, 2016)
        kOH = 1.34e-11;    % cm3 molec^-1 s^-1 (reaction rate constant for IVOCs; see Hodzic, ACP, 2016)
end

%% formation of oxidation products in C for each time step
dVOC = zeros(length(timeArray),1);
dOA = zeros(length(timeArray),length(C));

for i = 2:length(timeArray)
    dVOC(i,1) = ((-kOH * OH * VOC(i-1,1)) + VOC_prod) * dt;
    VOC(i,1) = VOC(i-1,1) + dVOC(i,1);
    
    dOA(i,:) = alpha * kOH * OH * VOC(i,1) * dt;
    dOA(i,:) = dOA(i,:) ./ 6.022e23 .* molmass .* 1e12; % convert to �g/m3
end

%% tempreature dependence (according to Donahue, EST, 2006: C*s are seperated by deltaHvap=5.8333 kJ/mol)
deltaHvap = 100 - log10(Cstars) * 5.8333;
R = 8.314/1000; %kJ/(mol K)

Cstars_T = Cstars .* (300 / Temp) .* exp(deltaHvap/R *(1/300 - 1/Temp));

%% kinetic transformation
% branching matrix
A = [1.0 0   0   0   0   0   0   0   0   0   0; ...
     0   0.4 0.4 0.4 0   0   0   0   0   0   0; ...
     0   0.5 0.4 0.4 0.4 0   0   0   0   0   0; ...
     0   0   0.1 0   0.4 0.4 0   0   0   0   0; ...
     0   0   0   0.1 0   0.4 0.4 0   0   0   0; ...
     0   0   0   0   0.1 0   0.4 0.4 0   0   0; ...
     0   0   0   0   0   0.1 0   0.4 0.4 0   0; ...
     0   0   0   0   0   0   0.1 0   0.4 0.4 0; ...
     0   0   0   0   0   0   0   0.1 0   0.4 0; ...
     0   0   0   0   0   0   0   0 0.1   0   0; ...
     0   0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.2 0];

I = eye(11);

% transformation matrix
K = kOH * OH * (A - I);

%% do aging and partitioning
dC = zeros(length(timeArray),length(C));
CsOA = zeros(length(timeArray),length(C));

    % First for time = 0:
    % Cumulative sum of total organics from low to high volatility
    sumConc_all = cumsum(C(1,:));

    xiGuess = sumConc_all ./ Cstars_T - 1;
    xiGuess = min(xiGuess,1);
    xiGuess = max(xiGuess,0);
    xi = xiGuess;

    % Determine the partitioning coefficient
    err = 1;
    maxErr = 1e-4;
    while err>maxErr
        CsOA(1,2:end) = C(1,2:end) .* xi(1,2:end);
        CsOA(1,1) = POA;
        C_OA(1,1) = sum(CsOA(1,:));
        xiNew = (1 + Cstars_T / C_OA(1,1)).^-1;
        err = max(abs(log(xi ./ xiNew)));
        xi = xiNew; % final partitioning coefficient
    end

    % ... and now do aging and partitioning for the following time steps:
for i = 2 : length(timeArray)
    dC(i,:) = - K * C(i-1,:)' * dt;
    C(i,:) = C(i-1,:) - dC(i,:) + dOA(i-1,:);
    
    % Cumulative sum of total organics from low to high volatility
    sumConc_all = cumsum(C(i,:));

    xiGuess = sumConc_all ./ Cstars_T - 1;
    xiGuess = min(xiGuess,1);
    xiGuess = max(xiGuess,0);
    xi = xiGuess;

    % Determine the partitioning coefficient
    err = 1;
    maxErr = 1e-4;
    while err>maxErr
        CsOA(i,2:end) = C(i,2:end) .* xi(1,2:end);   % mass of org. matter in each bin
        CsOA(i,1) = POA;
        C_OA(i,1) = sum(CsOA(i,:));
        xiNew = (1 + Cstars_T / C_OA(i,1)).^-1;
        err = max(abs(log(xi ./ xiNew)));
        xi = xiNew; % final partitioning coefficient
    end
end

SOAmass = C_OA(end,1);
VOCfinal = VOC(end,1);

%% plot figures

if printFigure == 1,
    figure('color',[1 1 1],'position',[159 160 560 420])
    plot(timeArray/3600,C_OA,'color',[0 .5 0],'linewidth',2)
    set(gca,'linewidth',2,'fontsize',14)
    hold
    plot(timeArray/3600,sum(C,2),'color',[.2 .2 .2],'linewidth',2,'linestyle',':')
    xlabel('time / hours','fontsize',14)
    ylabel('mass conc. / �g m^{-3}','fontsize',14)
    legend({'OA particle mass','total mass'},'location','northwest')
    title('SOA formation / �g m^{-3}')
    
    figure('color',[1 1 1],'position',[434 316 560 420])
    plot(timeArray/3600,VOC,'linewidth',2)
    set(gca,'linewidth',2,'fontsize',14)
    xlabel('time / hours','fontsize',14)
    ylabel('VOC conc. / molec. cm^{-3}','fontsize',14)
    title('VOC conc. / molec. cm^{-3}')
end
    
% figure
% bar(log10(Cstars_T(1,2:9)),C(length(timeArray)-1,2:9),'facecolor',[1 1 1])
% hold
% bar(log10(Cstars_T(1,2:9)),CsOA(length(timeArray)-1,2:9),'facecolor',[0 .5 0])
% 

end