function [POAconc,E_POA] = POA_Dp(start,stop,Dp)
% calculate daily POA concentration in the MBL

% according to Lana et al. (ACP, 2012) and Gantt et al. (ACP, 2012)

% we need the following variables loaded into the workspace: 
    % sea surface temperature [�C]:     'SST'
    % wind speed data [m/s]:            'U10'
    % chlorophyll a data [mg m-3]:      'chla'
    % boundary layer heights [m]:       'pbl'

% load data
currentfolder = evalin('base','currentfolder');

load([currentfolder, '\input data\SST'])
load([currentfolder, '\input data\wind'],'wind')
load([currentfolder, '\input data\pbl'])
load([currentfolder, '\input data\chla'])

pbl = pbl(1:180,:,:);


%% parameters
if nargin < 1, start = 1; end   % start of season
if nargin < 2, stop = 12; end   % end of season
if nargin < 3, Dp = 0.3; end      % if no particle diameter is given, we calculate it for 0.3 �m particles (as in Lana, ACP, 2012)

rho_salt = 2.165e-3;  % density of sea salt in ng �m-3

chla = nanmean(chla(:,:,start:stop),3);    % average chl-a data for the season
T = nanmean(SST(:,:,start:stop),3);        % average sea surface temperature for the season
U10 = nanmean(wind(:,:,start:stop),3);     % average wind speed for season
U22 = ((22/10)^0.11) * U10;                % calculate wind speed at 22m according to Lana, 2012 and Hsu, 1994
height = nanmean(pbl(:,:,start:stop),3);   % average PBL height for season

%% calculations

% A = 4.7 * (1 + 30 * (Dp/2))^(0.017*(Dp/2)^(-1.44));
% B = (0.433 - log10(Dp/2)) / 0.4333;
% C = ((0.3 + 0.1 .* T) - (0.0076 .* T.^2) + (0.00021 .* T.^3));
% 
% V_SSA = (pi / 6 *Dp.^3) .* C .* (1.373 .* U22.^3.41 .* (Dp/2).^-(A) .*...
%     (1 + 0.057 .* (Dp/2).^3.45) .* 10.^(1.607 .* exp(-B.^2))); % Sea spray volume emission rate, �m3 m-2 s-1 (#Gantt,2012)

V_SSA = 160.19 * U22.^2.706 * (pi / 6 *Dp.^3);    % Sea spray volume emission rate, 10^6 �m3 m-2 day-1 (#Lana,2012)

% E_salt = V_SSA .* rho_salt;  % Sea-salt mass emission rate, ng m-2 s-1 (#Gantt,2012)

OM_SSA = (1 ./ (1+exp(3.*(-2.63.*chla)+3*(0.18.*U22)))) ./ ...
    (1+0.03*exp(6.81*Dp)) + 0.03 ./ (1+exp(3.*(-2.63.*chla)+...
    3*(0.18.*U22))); % Organic mass fraction of sea spray aerosol, unitless (#Gantt,2012)

E_SSA = V_SSA .* (2.165 - 1.165 .* OM_SSA);   % mass flux of sea spray in acc. mode (#Lana,2012), �g m-2 d-1

E_POA = E_SSA .* OM_SSA;                      % mass flux of POA in acc. mode (#Lana,2012), �g m-2 d-1

%F_sup = 0.03 * (0.63*chla + 0.1);   % see Gantt, ACP, 2009

%rho_SSA = rho_salt ./ (1-F_sup*(1- rho_salt / 1e-3)); %(#Gantt,2012)

%E_POA = 6 .* V_SSA .* OM_SSA .* rho_SSA;    % Marine POA mass emission rate,  ng m-2 s-1 (#Gantt,2012)

%POAperday = E_POA * 3600 * 24;      % column mass of POA per day (#Gantt,2012), ng m-2
%POAconc = POAperday ./ height * 1e-3;  % POA mass concentration per day (#Gantt,2012), �g m-3

POAconc = E_POA ./ height;  % average POA mass concentration (#Lana, 2012), �g m-3

end
