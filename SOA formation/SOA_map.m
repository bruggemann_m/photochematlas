%% calculate potential SOA from precursor VOC 

function [OAmass,VOC,extraOA,rel_OAincrease,extraOAsum] = SOA_map(start,stop,SOAcase,agingTime,Temp,OH,yieldcase)

global colmap Lon Lat srfc_area

% standard parameters
if nargin < 1, start = 1; end
if nargin < 2, stop = 12; end
if nargin < 3, SOAcase = 1; end
if nargin < 4, agingTime = 12; end
if nargin < 5, Temp = 20; end
if nargin < 6, OH = 1.5e6; end
if nargin < 7, yieldcase = 'ole2'; end

yieldcase = lower(yieldcase);

% load data
currentfolder = evalin('base','currentfolder');

load([currentfolder, '\input data\POA.mat'],'POA');
if start>stop
    dummy(:,:,1:13-start) = POA(:,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = POA(:,:,1:stop);
    poa = nanmean(dummy,3);
else
    poa = nanmean(POA(:,:,start:stop),3);
end

load([currentfolder, '\input data\pbl.mat'],'pbl');
if start>stop
    dummy(:,:,1:13-start) = pbl(1:180,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = pbl(1:180,:,1:stop);
    pbl = nanmean(dummy,3);
else
    pbl = nanmean(pbl(1:180,:,start:stop),3);
end

% initialize and zero output matrix
OAmass = zeros(180,360);    % total mass concentration of OA particles (SOA + POA)

% choose between (1) emission case and (2) steady state case
switch SOAcase
    
    case 1
        % case 1: constant emission of isoprene due to photochemistry
        [E_VOC,~,~] = VOC_map(start,stop,'E_{VOC}'); % Emission of VOCs / molec. cm-2 day-1
            m_grid; m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);colorbar;
            title('E_{VOC} / molec. cm^{-2} day^{-1}')
        
        VOC = zeros(180,360);       % VOC concentration that is reached at the end of the aging time
        
        for lat=1:180
            for lon=1:360
                [OAmass(lat,lon),VOC(lat,lon)] = SOA_gridpoint(0,agingTime,Temp,poa(lat,lon),E_VOC(lat,lon),pbl(lat,lon),OH,0,yieldcase);
            end
        end
        
    case 2
        % case 2: final (daily) VOC mix. ratios are assessed for their SOA formation potential (less realistic!)
        % (no additional emissions during SOA aging; VOCs don't react immediately after emission, but concentrate in the atmosphere)
        [VOC,~] = VOC_ppt(start,stop,'VOC / ppt');
        
        for lat=1:180
            for lon=1:360
                [OAmass(lat,lon),~] = SOA_gridpoint(VOC(lat,lon)*1e-3,agingTime,Temp,poa(lat,lon),0,pbl(lat,lon),OH,0,yieldcase);
            end
        end
        
    otherwise
        error('Please choose case 1 our 2 when running SOA_map!')
end

extraOA = (OAmass - poa) * 1e3; % additional OA mass concentration / ng m-3
rel_OAincrease = extraOA ./ (poa * 1e3);    % relative increase in OA mass concentration

% calculate sum of additional OA mass from VOC oxidation for the selected season in g/season
dayspermonth = 30;
if start>stop
    dummy = nansum(nansum(extraOA.*srfc_area/1e4.*pbl,1),2)*((12-start+1)*dayspermonth)*1e-9;
    extraOAsum = dummy + nansum(nansum(extraOA.*srfc_area/1e4.*pbl,1),2)*(stop*dayspermonth)*1e-9;
else
    extraOAsum = nansum(nansum(extraOA.*srfc_area/1e4.*pbl,1),2)*((stop-start+1)*dayspermonth)*1e-9; % global additional OA in g / season
end

%% plot output using m_map toolbox

% figure('color',[1 1 1])
% m_pcolor(Lon,Lat,extraOA);shading flat;colormap(ppt_colors);   %plots additional OA mass conc. per day
% m_grid;
% m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);
% colorbar;
% title('additional OA mass / ng m^{-3}')
% 
% figure('color',[1 1 1])
m_pcolor(Lon,Lat,rel_OAincrease * 100);shading flat;colormap(colmap);   %plots VOC flux per day
% m_grid;
m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);
% colorbar;
% title('relative increase in OA mass conc. / %')

end