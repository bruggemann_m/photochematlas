%% startup the scripts

currentfolder = pwd;
if ~exist([pwd, '\output\'], 'dir')
  mkdir([pwd, '\output\']);
end
outputfolder = [pwd '\output\']; %define where to save the output files


addpath (currentfolder);
addpath ([pwd, '\input data'],'-end');
addpath ([pwd, '\SML coverage'],'-end');
addpath ([pwd, '\trophic states'],'-end');
addpath ([pwd, '\SOA formation'],'-end');
addpath ([pwd, '\VOC formation'],'-end');


load_parameters;
initialize;