function varargout = PhotochemAtLAS(varargin)

% PhotochemAtLAS MATLAB code for PhotochemAtLAS.fig
%      PhotochemAtLAS, by itself, creates a new PhotochemAtLAS or raises the existing
%      singleton*.
%
%      H = PhotochemAtLAS returns the handle to a new PhotochemAtLAS or the handle to
%      the existing singleton*.
%
%      PhotochemAtLAS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PhotochemAtLAS.M with the given input arguments.
%
%      PhotochemAtLAS('Property','Value',...) creates a new PhotochemAtLAS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the PhotochemAtLAS before PhotochemAtLAS_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PhotochemAtLAS_OpeningFcn via varargin.
%
%      *See PhotochemAtLAS Options on GUIDE's Tools menu.  Choose "PhotochemAtLAS allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PhotochemAtLAS

% Last Modified by GUIDE v2.5 06-Mar-2018 16:02:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PhotochemAtLAS_OpeningFcn, ...
                   'gui_OutputFcn',  @PhotochemAtLAS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PhotochemAtLAS is made visible.
function PhotochemAtLAS_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PhotochemAtLAS (see VARARGIN)

% Choose default command line output for PhotochemAtLAS
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PhotochemAtLAS wait for user response (see UIRESUME)
% uiwait(handles.figure1);

evalin('base','startup');
currentfolder = evalin('base','currentfolder');

axes(handles.axes1)
imshow([currentfolder, '\input data\logo_PhotochemAtLAS.png'])
axes(handles.axes2)
imshow([currentfolder, '\input data\logo_CNRS.jpg'])
axes(handles.axes3)
imshow([currentfolder, '\input data\logo_IRCELYON.jpg'])
axes(handles.axes4)
imshow([currentfolder, '\input data\logo_TROPOS.png'])

% --- Outputs from this function are returned to the command line.
function varargout = PhotochemAtLAS_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1

% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2

% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes3

% --- Executes during object creation, after setting all properties.
function axes4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes4

% --- Executes on button press in buttonSML.
function buttonSML_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSML (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of buttonSML
set(handles.inputagingtime,'Enable','off')
set(handles.inputtemperature,'Enable','off')
set(handles.listSOAyield,'Enable','off')
set(handles.molmassinput,'Enable','off')
set(handles.VOCflxinput,'Enable','off')

% --- Executes on button press in buttonpotential.
function buttonpotential_Callback(hObject, eventdata, handles)
% hObject    handle to buttonpotential (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of buttonpotential
set(handles.inputagingtime,'Enable','off')
set(handles.inputtemperature,'Enable','off')
set(handles.listSOAyield,'Enable','off')
set(handles.molmassinput,'Enable','off')
set(handles.VOCflxinput,'Enable','off')


% --- Executes on button press in buttonVOC.
function buttonVOC_Callback(hObject, eventdata, handles)
% hObject    handle to buttonVOC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of buttonVOC
set(handles.inputagingtime,'Enable','off')
set(handles.inputtemperature,'Enable','off')
set(handles.listSOAyield,'Enable','off')
set(handles.molmassinput,'Enable','on')
set(handles.VOCflxinput,'Enable','on')

% --- Executes during object creation, after setting all properties.
function buttonseasonal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to buttonseasonal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function buttonmonthly_CreateFcn(hObject, eventdata, handles)
% hObject    handle to buttonmonthly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes on button press in buttonmonthly.
function buttonmonthly_Callback(hObject, eventdata, handles)
% hObject    handle to buttonmonthly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of buttonmonthly
set(handles.monthinput,'Enable','on')
set(handles.checkbox_addfig,'Value',0)
set(handles.checkbox_addfig,'Enable','on')

% --- Executes on button press in buttonseasonal.
function buttonseasonal_Callback(hObject, eventdata, handles)
% hObject    handle to buttonseasonal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of buttonseasonal
set(handles.monthinput,'Enable','off')
set(handles.checkbox_addfig,'Value',1)
set(handles.checkbox_addfig,'Enable','off')

function monthinput_Callback(hObject, eventdata, handles)
% hObject    handle to monthinput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of monthinput as text
%        str2double(get(hObject,'String')) returns contents of monthinput as a double


% --- Executes during object creation, after setting all properties.
function monthinput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to monthinput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function wliminput_Callback(hObject, eventdata, handles)
% hObject    handle to wliminput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of wliminput as text
%        str2double(get(hObject,'String')) returns contents of wliminput as a double


% --- Executes during object creation, after setting all properties.
function wliminput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to wliminput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function molmassinput_Callback(hObject, eventdata, handles)
% hObject    handle to molmassinput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of molmassinput as text
%        str2double(get(hObject,'String')) returns contents of molmassinput as a double


% --- Executes during object creation, after setting all properties.
function molmassinput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to molmassinput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VOCflxinput_Callback(hObject, eventdata, handles)
% hObject    handle to VOCflxinput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VOCflxinput as text
%        str2double(get(hObject,'String')) returns contents of VOCflxinput as a double



% --- Executes during object creation, after setting all properties.
function VOCflxinput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VOCflxinput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function savename_Callback(hObject, eventdata, handles)
% hObject    handle to savename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of savename as text
%        str2double(get(hObject,'String')) returns contents of savename as a double

% --- Executes during object creation, after setting all properties.
function savename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to savename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function inputagingtime_Callback(hObject, eventdata, handles)
% hObject    handle to inputagingtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputagingtime as text
%        str2double(get(hObject,'String')) returns contents of inputagingtime as a double


% --- Executes during object creation, after setting all properties.
function inputagingtime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputagingtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function inputtemperature_Callback(hObject, eventdata, handles)
% hObject    handle to inputtemperature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inputtemperature as text
%        str2double(get(hObject,'String')) returns contents of inputtemperature as a double


% --- Executes during object creation, after setting all properties.
function inputtemperature_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inputtemperature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listSOAyield.
function listSOAyield_Callback(hObject, eventdata, handles)
% hObject    handle to listSOAyield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listSOAyield contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listSOAyield


% --- Executes during object creation, after setting all properties.
function listSOAyield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listSOAyield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonSOA.
function buttonSOA_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSOA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of buttonSOA
set(handles.inputagingtime,'Enable','on')
set(handles.inputtemperature,'Enable','on')
set(handles.listSOAyield,'Enable','on')
set(handles.molmassinput,'Enable','on')
set(handles.VOCflxinput,'Enable','on')


% --- Executes during object creation, after setting all properties.
function textWait_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textWait (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in runbutton.
function runbutton_Callback(hObject, eventdata, handles)
% hObject    handle to runbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.textWait,'Visible','on')

global wlim molmass VOC_flx

wlim = str2double(get(handles.wliminput,'String'));
molmass = str2double(get(handles.molmassinput,'String'));
VOC_flx = str2double(get(handles.VOCflxinput,'String'))*1e7;

initialize(wlim,molmass,VOC_flx);
axes(handles.axes1)

buttonpotential = get(handles.buttonpotential,'Value');
buttonSML = get(handles.buttonSML,'Value');
buttonVOC = get(handles.buttonVOC,'Value');
buttonSOA = get(handles.buttonSOA,'Value');
namemonth = {'Jan';'Feb';'Mar';'Apr';'May';'Jun';'Jul';'Aug';'Sep';'Oct';'Nov';'Dec';};

addfig = get(handles.checkbox_addfig, 'Value');

if buttonVOC == 1
    %calculate VOC emissions
    if get(handles.buttonmonthly,'Value') == 1
        range = get(handles.monthinput,'String');
        if length(range)>2
            start = str2double(range(1:2));
            stop = str2double(range(4:5));
        else
            start = str2double(range);
            stop = str2double(range);
        end
        
        if addfig == 1
            figure('color',[1 1 1],'units','centimeters','position',[3 3 16 9],...
            'paperunits','centimeters','paperposition',[0 0 16 9],...
            'papersize',[16 9])
        end
        
        header = [char(namemonth(start)) ' - ' char(namemonth(stop))];
        [map,VOCsum,~] = VOC_map(start,stop,header);
        m_grid;
        h=colorbar('h','eastoutside');
        set(get(h,'title'),'string','VOC flux / molec./cm^2/day','rotation',90,...
            'fontsize',11,'position',[55.75 100]);
        
        assignin('base','map',map)
        
    elseif get(handles.buttonseasonal,'Value') == 1
        
        VOCsum = VOC_seasonal_flux;
        
    end
    
elseif buttonpotential == 1
    %calculate photochemical potential
    if get(handles.buttonmonthly,'Value') == 1
        range = get(handles.monthinput,'String');
        if length(range)>2
            start = str2double(range(1:2));
            stop = str2double(range(4:5));
        else
            start = str2double(range);
            stop = str2double(range);
        end
        
        if addfig == 1
            figure('color',[1 1 1],'units','centimeters','position',[3 3 16 9],...
            'paperunits','centimeters','paperposition',[0 0 16 9],...
            'papersize',[16 9])
        end
        
        header = [char(namemonth(start)) ' - ' char(namemonth(stop))];
        [map,potential_sum,~] = VOC_map_potential(start,stop,header);
        m_grid;
        h=colorbar('h','eastoutside');
        set(get(h,'title'),'string','\it�_{photo} \rm/ mW s cm^{-2} day^{-1}','rotation',90,...
            'fontsize',11,'position',[55.75 100]);
        
        assignin('base','map',map)
        
    elseif get(handles.buttonseasonal,'Value') == 1
        
        VOC_seasonal_potential_plot(wlim);
        
    end
elseif buttonSML == 1
    %calculate SML coverage
    
    currentfolder = evalin('base','currentfolder');
    load([currentfolder, '\input data\SML_colormap.mat']);
    
    if get(handles.buttonmonthly,'Value') == 1
        range = get(handles.monthinput,'String');
        if length(range)>2
            start = str2double(range(1:2));
            stop = str2double(range(4:5));
        else
            start = str2double(range);
            stop = str2double(range);
        end
        
        if addfig == 1
            figure('color',[1 1 1],'units','centimeters','position',[3 3 16 9],...
            'paperunits','centimeters','paperposition',[0 0 16 9],...
            'papersize',[16 9])
        end        
        
        header = [char(namemonth(start)) ' - ' char(namemonth(stop))];
        map = SML_subfigure(start,stop,wlim);
        m_grid;
        caxis([0 3]);
        colormap(flipud(c));
        h = colorbar('h','eastoutside','ticks',[0:3],...
            'ticklabels',flip({'high','medium','low','none'}),'fontsize',10);
        set(get(h,'title'),'string','SML coverage','rotation',90,...
            'fontsize',11,'position',[75 100]);
        
        assignin('base','map',map)
        
    elseif get(handles.buttonseasonal,'Value') == 1
        
        SML_seasonal(wlim);
        
    end
elseif buttonSOA == 1
    %calculate additional SOA mass concentration
    
    set(handles.textWait,'Visible','on')
    
    agingTime = str2double(get(handles.inputagingtime,'String'));
    Temp = str2double(get(handles.inputtemperature,'String'));
    yieldcase = get(handles.listSOAyield,'Value');
        if yieldcase == 1, yieldcase = 'isop';
        elseif yieldcase == 2, yieldcase = 'ole2';
        elseif yieldcase == 3, yieldcase = 'ivoc'; 
        end
    
    if get(handles.buttonmonthly,'Value') == 1
        range = get(handles.monthinput,'String');
        if length(range)>2
            start = str2double(range(1:2));
            stop = str2double(range(4:5));
        else
            start = str2double(range);
            stop = str2double(range);
        end
        
        if addfig == 1
            figure('color',[1 1 1],'units','centimeters','position',[3 3 16 9],...
            'paperunits','centimeters','paperposition',[0 0 16 9],...
            'papersize',[16 9])
        end        

        header = [char(namemonth(start)) ' - ' char(namemonth(stop))];
        [~,~,~,rel_OAincrease,~] = SOA_map(start,stop,1,agingTime,Temp,1.5e6,yieldcase);
        map = rel_OAincrease;
        title(header)
        m_grid;
        h=colorbar('h','eastoutside');
        set(get(h,'title'),'string','rel. OA mass increase / %','rotation',90,...
            'fontsize',11,'position',[55.75 100]);

        assignin('base','map',map)

    elseif get(handles.buttonseasonal,'Value') == 1

        SOA_seasonal(agingTime,Temp,yieldcase);


    end
    
end

set(handles.textWait,'Visible','off')

% --- Executes on button press in savebutton.
function savebutton_Callback(hObject, eventdata, handles)
% hObject    handle to savebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

outputfolder = evalin('base','outputfolder');
map = evalin('base','map');

filename = get(handles.savename,'String');
save([outputfolder filename '.txt'],'map','-ascii');
display(['Done! Saved in outputfolder as: ' filename '.txt'])


% 21/08/2017

% --- Executes on button press in checkbox_addfig.
function checkbox_addfig_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_addfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_addfig
