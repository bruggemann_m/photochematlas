%% create workspace with net primary productivity data (= "biological activity")
%  data were downloaded from http://orca.science.oregonstate.edu/1080.by.2160.monthly.hdf.vgpm.m.chl.m.sst.php

% after data import from hdf-files for each month the following procedure
% is performed to reduce the grid resolution to 1 degree

C = char(who);  %to obtain the names of the data matrices for each year

start = input('\nData is starting in which year? [1-99]: ');  %all data are after 2000
stop = input('Data is ending in which year? [1-99]: ');

npp=nan(180,360,stop);  % = latitude, longitude, year

j=1;
for i=start:stop
    npp(:,:,i)=lowres_npp(eval(C(j,:)));      %calls lowres_npp function
    j=j+1;
end

avg_npp = nanmean(npp(:,:,start:stop),3);

ask = input('\nDo you want to save the workspace? [y/n]: ','s');
if isempty(ask) | ask=='y'
    name = input('\nUnder which name do you want to store the workspace? ','s');
    save(name);
end

clear i j C start stop ask name