function npp = lowres_npp(npp_matrix)

% reduce npp-matrix to 1-degree resolution

npp_matrix(npp_matrix==-9999)=nan;

idx_clmn = [1:6:2160];
idx_row = [1:6:1080];

npp_1deg_lon = zeros(1080,360);
npp_1deg = zeros(180,360);

for i=1:360
    for j=1:1080
        npp_1deg_lon(j,i) = nanmean(npp_matrix(j,idx_clmn(1,i):idx_clmn(1,i)+5));
    end
end

for j=1:360
    for i=1:180
        npp_1deg(i,j) = nanmean(npp_1deg_lon(idx_row(1,i):idx_row(1,i)+5,j));
    end
end

npp = npp_1deg;

end