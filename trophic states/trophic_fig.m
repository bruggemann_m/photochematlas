function trophic_fig(start,stop)
%% prepare figure of trophic regions

if nargin < 1; start=1; end
if nargin < 2; stop=12; end

currentfolder = evalin('base','currentfolder');

trophic = trophic_regions;

if start>stop
    dummy(:,:,1:13-start) = trophic(:,:,start:12);
    dummy(:,:,14-start:14-start+stop-1) = trophic(:,:,1:stop);
    avg_troph = nanmean(dummy,3);
else
    avg_troph = nanmean(trophic(:,:,start:stop),3);
end

figure('color',[1 1 1])
[Plg,Plt]=meshgrid([-180:179]-0.5,[-89:90]-0.5);

m_pcolor(Plg,Plt,flipud(avg_troph));shading flat;colormap(flipud(jet));
m_coast('patch',[0 0 0],'edgecolor',[0 0 0]);
m_grid;

month = {'Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Oct' 'Nov' 'Dec'};
title(['trophic states for: ' cell2mat(month(1,start)) '-' cell2mat(month(1,stop))])

h=colorbar('h','eastoutside','ticks',[-3 -2 -1 0],'ticklabels',{'eu-','meso-','oligo-','n/a'},'fontsize',10);

prompt = input('Do you want to save the figure? y/n [n]: ','s');
if prompt == 'y'
    d = char(datetime('today','format','yyMMdd'));
    outputfolder = evalin('base','outputfolder');
    filename = ['trophic_' cell2mat(month(1,start)) '-' cell2mat(month(1,stop))];
    print([outputfolder d '_' filename '.png'],'-dpng','-r300')
    saveas(gcf,[outputfolder d '_' filename '.fig'],'fig')
end

end