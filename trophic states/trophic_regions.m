function trophic = trophic_regions
%% define oligo-, meso-, and eutrophic regions

currentfolder = evalin('base','currentfolder');

% load data for net primary productivity
load([currentfolder, '\input data\NPP.mat'])

% get variables
vars = char(who);

% define trophic states
trophic(:,:,12) = zeros(180,360);
mat = zeros(180,360);

% trophic regions are defined according to the limits given 
% by Wurl, Biogeosciences, 2011:
%
%   oligotrophic:   <400 mg C/m�/day
%   mesotrophic:    400...1200 mg C/m�/day
%   eutrophic:      >1200 mg C/m�/day

for i=1:12
    mat(eval(vars(i,:))<400)=-1;   %for oligotrophic regions -1 
    mat(eval(vars(i,:))>=400)=-2;  %for mesotrophic regions -2
    mat(eval(vars(i,:))>1200)=-3;  %for eutrophic regions -3
    trophic(:,:,i)=mat;
end

clear i mat vars

%save([currentfolder, '\trophic states\170223_trophic_regions.mat'])
end