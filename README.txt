Guide to the 

Photochemistry At Liquid/Air interfaces containing Surfactants 

(PhotochemAtLAS) model

***************************************************************************

   Welcome! The code presented here models the photochemical VOC production
   from biogenic surfactants at the air/water interface of the oceans.
   Moreover, SOA formation upon oxidation of these VOCs can be estimated
   using a one-dimensional volatility basis set approach.

    I.	  Basic Instructions
         i)     PhotochemAtLAS - the Graphical User Interface
        ii)	The VOC emission module
       iii)	The SOA formation module
        iv)     Batch mode
    II.	  Code Dependencies
    III.  Additional Functionalities


***************************************************************************
I.   Basic Instructions

Before running the model, please make sure that the m_map toolbox is
installed and initialized in Matlab. The m_map toolbox is available free of
charge at: https://www.eoas.ubc.ca/~rich/map.html

---
I.i  PhotochemAtLAS - the Graphical User Interface

The most user-friendly way of running the PhotochemAtLAS model is to use 
the graphical user interface provided with the code. To do so, please run 
the 'PhotochemAtLAS.m' script, either by typing 'PhotochemAtLAS' into the
MATLAB command window (without '') or by selecting the 'PhotochemAtLAS.m'
script in MATLAB folder window and pressing F9. 

A new window will open where you can choose between calculation of SML 
coverage, photochemical potentials (µ_photo), VOC emissions, or additional 
SOA mass. Furthermore, you can select between a monthly calculation or the 
production of a seasonal overview. 

Please modify the values for SML wind speed limit, average molecular 
weight, and VOC production as desired. (Values for molecular weigth and VOC
production will only have an effect on calculation of VOC emissions and 
additional SOA formation.) If you want to calculate additional SOA mass
due to oxidation of photochemically produced VOCs, please also enter values
for aerosol aging time and ambient air temperature in the SOA settings 
panel. Moreover, please select an SOA yield scenario.

Then click on the 'Run calculations now!' button. If you selected a monthly
calculation the generated map will be saved in MATLAB's workspace as a 
variable named 'map'. Moreover, you have the possibility to show the output
in a new window by checking the box next to the run button. This will allow
you to save the figure in any format and resolution, or even to modify it 
before saving. In addition, you can also save the produced map in 
txt-format to the output folder by choosing a filename and clicking the 
'Save now!' button. Please note that existing files will be overwritten 
without prior warning if duplicate names are entered.

---
I.ii  The VOC emission module

More flexibility is available by running the photochemATLAS scripts from
the MATLAB command line. To do so,

    1) run 'startup.m' to load all parameters and to define the paths
    2) execute the script 'runVOCmodel'

In the 'runVOCmodel' we define the windspeed limit for the formation of an
SML on the surface of the ocean. The value is saved in the global variable
'wlim'. According to Sabbaghzadeh (GRL, 2017), the standard is set 
to 13 m/s. However, other values can be used easily, since most functions
allow a re-definition of 'wlim' upon execution.

We define the molar mass (g/mol) and the VOC production (in molecules mW-1 
s-1) in the 'runVOCmodel' script. Please change these values as necessary 
when calling the function. If no further input is given wlim is set to 
13 m s-1 and the calculation is conducted for isoprene emissions from SML
samples (see Ciuraru et al., EST, 2015).

All further (global) parameters are set in the load_parameters function.
Changes to these parameters might, however, have profound consequences to
the provided scripts and functions.

After running the script, figures can easily be saved to the output folder
by running 'saveplot.m'. In addition, the calculated VOC emissions can be
saved in a text file. However, to do so, you have to uncomment several 
lines in the 'saveplot' script.

If maximum mixing ratios for the VOCs are desired, please run 'VOC_ppt' or 
'VOC_seasonal_ppt' afterwards. Mixing ratios are given in pptv and assume
an instantaneous mixing of the emitted VOC within the planetary boundary
layer of the given grid point. In addition, we assume that neither
oxidation nor transport between grid points occurs. Planetary boundary 
layer heights are monthly long term means taken from 'NOAA-CIRES 20th 
Century Reanalysis version 2c'.

---
I.iii  The SOA formation module

To run the SOA formation module start the script 'runSOAmodel'. If the VOC
model was already run before and no other input is given, the script will
take the previously defined parameters to calculate additional OA mass 
from the emitted VOCs for each season.

The SOA calculations are based on partitioning theory and the 1dim-VBS
(Pankow, EST, 1994; Odum, EST, 1996; Donahue, EST, 2006). In the standard 
configuration SOA yields for unsaturated VOCs are used (Tsimpidi, ACP, 
2010). If you want to conduct calculations for other VOCs, please change
the parameters accordingly when calling the function.

After running the script, figures can easily be saved to the output folder
by running 'saveplot.m'. In addition, the calculated SOA mass conc. can be
saved in a text file. However, to do so, you have to uncomment several 
lines in the 'saveplot' script.

--
I.iii Batch mode

We can calculate VOC emissions and SOA formation for different scenarios 
in batch mode by executing 'batch_VOCandSOA.m'. When calling the function
we can choose between the 'ole2' and the 'isop' case. 'ole2' calculates
emissions of unsaturated compounds and SOA formation according to 
Tsimpidi et al. (ACP, 2010). 'isop' calculates emissions of isoprene and 
SOA formation according to Hodzic et al. (ACP, 2016).


***************************************************************************
II. Code Dependencies

startup;
  load_parameters;
  initialize;

---
The code dependencies of the VOC emission module are as follows:

runVOCmodel;
  VOC_seasonal_flux;
      VOC_map;
          SML_coverage;
	      trophic_regions;

VOC_seasonal_potential;
  VOC_map_potential;
      SML_coverage;
	  trophic_regions;

VOC_seasonal_ppt;
  VOC_ppt;
      VOC_map;
	SML_coverage;
	      trophic_regions;


---
The code dependencies of the SOA formation module are as follows:

runSOAmodel;
  SOAseasonal;
      SOA_map;
        ->case 1:    VOC_map;
                       SML_coverage;
                         trophic_regions;
                     SOA_gridpoint;
        ->case 2:    VOC_ppt;
	  	       VOC_map;
	  	         SML_coverage;
	   	         trophic_regions;
                     SOA_gridpoint;

POAmonthly;
  POA_Dp;



***************************************************************************
III. Additional Functionalities

a) run the VOC/SOA model for a specific time period only:
    To calculate daily VOC emissions only for a specific period of a year, 
    we can use 'VOC_map.m'. (Additionally, 'VOC_ppt' can be used to
    estimate monthly averages for VOC mixing ratios, resulting from the 
    modeled VOC emissions.) Similar to this, additional OA mass from SOA
    formation can be calculmated using 'SOA_map.m'.

b) calculate seasonal SML coverages:
    Using 'SML_figure.m' (and embedded in this 'SML_coverage.m'), we can
    estimate the presence of SMLs on the ocean's surface.
    To use it we only have to give the starting and ending month of the
    selected season, and the windspeed limit. 
    For example, 'SML_figure(1,3,12)' gives the average SML coverage for 
    Jan-Mar at a windspeed limit of 12 m/s. 
    (Please note: The windspeed limit is set here individually and is not
    predefined as a global variable.)
    To prepare a seasonal overview, please use 'SML_seasonal' and indicate
    the windspeed limit when calling the function, e.g., 'SML_seasonal(13)'
    will give the seasonal SML coverage for wlim=13 m/s.

c) calculate trophic states:
    Using 'trophic_fig.m', we can derive trophic states for each season.
    For example, 'trophic_fig(1,3)' gives the average trophic states for
    the season Jan-Mar.

***************************************************************************
created by
Martin Brüggemann, CNRS-IRCELYON, 2017